
import UIKit

class InstructionView: UIViewController
{
  
  var pageIndex : Int = 0
  var titleText : String = ""
  var imageFile : String = ""
    
  var newNavBar: UINavigationBar!
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    self.edgesForExtendedLayout = UIRectEdge.None;
    
    self.navigationController?.navigationBar.hidden = true
    
    newNavBar = UINavigationBar.self.init(frame: CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 64.0))
    newNavBar.translucent = false
    newNavBar.setTitleVerticalPositionAdjustment(-10, forBarMetrics: .Default)
    
    view.backgroundColor = UIColor(patternImage: UIImage(named: imageFile)!)
    
    let label = UILabel(frame: CGRectMake(0, 0, view.frame.width, 200))
    label.textColor = UIColor.whiteColor()
    label.text = titleText
    label.textAlignment = .Center
    view.addSubview(label)
    
    let navItem = UINavigationItem()
    navItem.title = titleText
   
    
    newNavBar.items = [navItem]
    
    self.view.addSubview(newNavBar)
    


  }
    
    
  override func didReceiveMemoryWarning()
  {
    super.didReceiveMemoryWarning()
  }
  
}
