
import UIKit


class NewUIPageViewController: UIPageViewController {

    override func viewDidLayoutSubviews() {

        for subView in self.view.subviews as [UIView] {
            if subView is UIScrollView {
                subView.frame = self.view.bounds
                
            } else if subView is UIPageControl {
                
                subView.frame = CGRectMake(0,40,self.view.frame.width,20.0)
                subView.backgroundColor = UIColor(white:1, alpha:0)
                self.view.bringSubviewToFront(subView)
    
                println(subView.frame)

            }
        }
        super.viewDidLayoutSubviews()
    }
    
}

class ViewController: UIViewController, UIPageViewControllerDataSource
{
    
  var pageViewController : NewUIPageViewController?
  var pageTitles : Array<String> = ["God vs Man", "Cool Breeze", "Fire Sky"]
  var pageImages : Array<String> = ["page1.png", "page2.png", "page3.png"]
  var currentIndex : Int = 0
  
  //var navController: UINavigationController!
  var pageControl: UIPageControl!
 
  var newNavBar: UINavigationBar!
    
   var navItem: UINavigationItem!
    
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    self.edgesForExtendedLayout = UIRectEdge.None;
    
    self.navigationController?.navigationBar.hidden = true
    
    pageViewController = NewUIPageViewController(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
    
    pageViewController!.dataSource = self
    
    pageViewController!.viewDidLayoutSubviews()

    
    let startingViewController: InstructionView = viewControllerAtIndex(0)!
    
    let viewControllers: NSArray = [startingViewController]
    pageViewController!.setViewControllers(viewControllers, direction: .Forward, animated: false, completion: nil)
    pageViewController!.view.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
    
    addChildViewController(pageViewController!)

    
    view.addSubview(pageViewController!.view)
    
    
    pageViewController!.didMoveToParentViewController(self)
    
    let gradient : CAGradientLayer = CAGradientLayer()
    gradient.frame =  CGRectMake(0, 0, view.frame.size.width, 60)
    gradient.startPoint = CGPoint(x: 0,y: 1)
    gradient.endPoint = CGPoint(x:1, y:1)

    gradient.locations = [0.2,0.3,0.5,0.7,0.8]
    let cor1 = UIColor(white: 1, alpha: 1).CGColor
    let cor2 = UIColor(white: 1, alpha: 0).CGColor
    let cor3 = UIColor(white: 1, alpha: 0).CGColor
    let cor4 = UIColor(white: 1, alpha: 0).CGColor
    let cor5 = UIColor(white: 1, alpha: 1).CGColor

    let arrayColors = [cor1, cor2, cor3, cor4, cor5]
    gradient.colors = arrayColors
    
    var DynamicView=UIView(frame: CGRectMake(0, 0, 70, 60))
    
    DynamicView.layer.insertSublayer(gradient, atIndex:0)

    self.view.addSubview(DynamicView)

    
  }
    
   override func viewWillAppear(animated: Bool) {
    
    println ("View Appears");
    
    }


    
  override func didReceiveMemoryWarning()
  {
    super.didReceiveMemoryWarning()
  }
  
  func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
  {
    var index = (viewController as InstructionView).pageIndex
    
      if (index == 0) || (index == NSNotFound) {
      return nil
    }
    
    index--
    
    return viewControllerAtIndex(index)
  }
  
  func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
  {
    var index = (viewController as InstructionView).pageIndex
    
       if index == NSNotFound {
      return nil
    }
    
    index++
    
    if (index == self.pageTitles.count) {
      return nil
    }
    
    return viewControllerAtIndex(index)
  }
  
  func viewControllerAtIndex(index: Int) -> InstructionView?
  {
    if self.pageTitles.count == 0 || index >= self.pageTitles.count
    {
      return nil
    }
    
    println("page turned")
    
    // Create a new view controller and pass suitable data.
    let pageContentViewController = InstructionView()
    pageContentViewController.imageFile = pageImages[index]
    pageContentViewController.titleText = pageTitles[index]
    pageContentViewController.pageIndex = index
    currentIndex = index
    
    return pageContentViewController
  }
  
  func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int
  {
    return self.pageTitles.count
  }
  
  func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int
  {
    return 0
  }
  
}
